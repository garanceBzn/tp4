function showHideDescription(){
    $(this).find(".description").toggle();
}

$(document).ready(function () {
    $('.pizza-type label').hover(
        showHideDescription,
        showHideDescription
        
    );
    
    $('.nb-parts input').on('keyup', function() {

    $(".pizza-pict").remove();
    var pizza = $('<span class="pizza-pict"></span>');

    slices = +$(this).val();

    for(i= 0; i<slices/6; i++){

        $(this).after(pizza.clone().addClass('pizza-6'));

    }

    if(slices%6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-'+slices%6); 
    });
    
    $('.next-step').click(function(){
        $(this).remove();
        $('.infos-client').show();
    });
    
    const input = $('<br><input type="text"/>');
    
    $('.add').click(function(){
        $(this).before(input.clone());
        
    });
    
    $('.done').click(function(){
        const name = $('#name').val();
        $('body').replaceWith('<p>Merci '+ name +' ! Votre commande sera livrée dans 15 minutes</p>');
        //$('body').remove();
        //$("Message").insertAfter('.done');

        //alert("Merci Garance ! Votre commande sera livrée dans 15 minutes")
    });

    function total(fPizzaPrix, fPateprix, fExtraPrix){
        total = fPizzaPrix + fPateprix + fExtraPrix;
        if (total==0){
            $('.stick-right p').text("0€");
        }
        else if (total!=0){
            $('.stick-right p').text(total.toPrecision(3)+ "€");
        }
    }
    
    $('.pizza-type input').click(function(){
        if ("input [type = 'radio'][name ='type']:checked"){
            PizzaPrix=0;
         PizzaPrix = $(this).data('price');
         PizzaPrix = (PizzaPrix/6)*slices;
        
        total (PizzaPrix, PatePrix, ExtraPrix);
    }
    });

    $(".type input[name ='pate']").click(function(){
        
        PatePrix =0;     
        PatePrix = $(this).data('price');
            
             total (PizzaPrix, PatePrix, ExtraPrix);
        
    });

    $(".type input[name ='extra']").click(function(){
        
            ExtraPrix=0;
            $('input [name =extra]:checked').each(function(){
                ExtraPrix = $(this).data('price');
            });
            total (PizzaPrix, PatePrix, ExtraPrix);
        
    });

});